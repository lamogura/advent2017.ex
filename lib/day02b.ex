defmodule Day2B do

  def calculate_checksum(matrix) do
    Enum.reduce(matrix, 0, &(row_checksum(&1) + &2))
  end
  
  defp row_checksum(row) do
    find_no_remainder_result(row, 0)
  end

  defp find_no_remainder_result(row, index) when index < length(row) do
    {num, rest} = List.pop_at(row, index)
    
    case Enum.find(rest, :not_found, &(rem(&1, num) == 0)) do
      :not_found ->
        find_no_remainder_result(row, index + 1)

      found_num ->
          div(found_num, num)
    end
  end
  
end