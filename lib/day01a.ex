defmodule Day1A do

  def calculate_sum(captcha) when is_binary(captcha) do
    captcha
    |> String.codepoints
    |> Enum.map(&String.to_integer/1)
    |> append_first_to_end
    |> calculate_sum(0)
  end

  defp append_first_to_end(a_list) do
    a_list ++ [List.first(a_list)]
  end
  
  defp calculate_sum([first, second | rest], sum) do
    if first == second do
      calculate_sum([second | rest], sum + first)
    else
      calculate_sum([second | rest], sum)
    end
  end

  defp calculate_sum([_last], sum) do
    sum
  end

end