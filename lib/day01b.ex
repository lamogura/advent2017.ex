defmodule Day1B do

  def calculate_sum(captcha) when is_binary(captcha) do
    captcha
    |> String.codepoints
    |> Enum.map(&String.to_integer/1)
    |> calculate_sum(0, 0)
  end
  
  defp calculate_sum(all, index, sum) do
    calculate_sum(all, index, Enum.count(all), sum)
  end

  defp calculate_sum(all, index, count, sum) when index < count do
    halfway_around_index = rem(index + div(count, 2), count)
    num = Enum.at(all, index)
    
    if num == Enum.at(all, halfway_around_index) do
      calculate_sum(all, index + 1, sum + num)
    else
      calculate_sum(all, index + 1, sum)
    end
  end

  defp calculate_sum(_all, _index, _count, sum) do
    sum
  end
  
end