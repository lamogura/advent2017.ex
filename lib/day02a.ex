defmodule Day2A do

  def calculate_checksum(matrix) do
    Enum.reduce(matrix, 0, &(row_checksum(&1) + &2))
  end
  
  defp row_checksum(row) do
    row_minmax_diff(row)
  end

  defp row_minmax_diff(row) do
    Enum.max(row) - Enum.min(row)
  end
  
end