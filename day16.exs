defmodule Day16 do
  defp spin(num_times, dancers) do
    dancers
    |> Enum.reduce(dancers, fn {key, _val}, dict ->
      Map.update!(dict, key, &rem(&1 + num_times, Enum.count(dancers)))
    end)
  end

  defp exchange([a_val, b_val], dancers) do
    with {a, _} <- Enum.find(dancers, nil, fn {_k, v} -> v == a_val end),
         {b, _} <- Enum.find(dancers, nil, fn {_k, v} -> v == b_val end) do
      %{dancers | a => b_val, b => a_val}
    else
      _ -> dancers
    end
  end

  defp swap([a, b], dancers) do
    a_val = Map.get(dancers, a)
    b_val = Map.get(dancers, b)
    %{dancers | a => b_val, b => a_val}
  end

  defp exec_cmd({"s", num_times_str}, dancers) do
    num_times_str
    |> String.to_integer()
    |> spin(dancers)

    # |> printit
  end

  defp exec_cmd({"x", values}, dancers) do
    values
    |> String.split("/")
    |> Enum.map(&String.to_integer/1)
    |> exchange(dancers)

    # |> printit
  end

  defp exec_cmd({"p", keys}, dancers) do
    keys
    |> String.split("/")
    |> swap(dancers)

    # |> printit
  end

  def start(_cmds, dancers, 0), do: dancers

  def start(cmds, dancers, repeat) when repeat > 0 do
    dancers =
      dancers ||
        "abcdefghijklmnop"
        |> String.codepoints()
        |> Enum.with_index()
        |> Map.new()
        |> printit

    new_dancers =
      cmds
      |> Enum.reduce(dancers, &exec_cmd(&1, &2))

    start(cmds, new_dancers, repeat - 1)
  end

  def printit(dancers) do
    dancers
    |> Map.to_list()
    |> List.keysort(1)
    |> Enum.map(fn {k, _v} -> k end)
    |> to_string
    |> IO.inspect()

    dancers
  end
end

# "s1,x3/4,pe/b"
case File.read("./day16.txt") do
  {:ok, body} ->
    body
    |> String.split(",")
    |> Enum.map(&String.split_at(&1, 1))
    |> Day16.start(nil, 1_000)
    |> Day16.printit()

  {:error, reason} ->
    IO.puts(reason)
end
